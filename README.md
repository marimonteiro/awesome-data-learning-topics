1. O senhor Ambrósio (sim, ele mesmo, do frigorífico) está de volta e mais exigente. Ele precisa de um sistema,
em Python, que tenha as seguintes funcionalidades:
a. Possibilite o cadastro de bois e persista isso em um arquivo .txt ou .json local na pasta aplicação. Para
o cadastro de um boi, deverá ser informado seu código (uma string única com quatro caracteres), seu
nome (uma string livre) e o peso (número inteiro).
b. Calcule qual o boi mais gordo que existe.
c. Calcule qual o boi mais magro que existe.
Isso deve acontecer da seguinte forma:

A aplicação deverá abrir um menu, onde o usuário deverá escolher entre cadastrar um novo boi,
calcular o boi mais gordo e calcular o boi mais magro.
Caso a primeira opção seja escolhida, o usuário deverá entrar com as informações do boi.
Caso as demais sejam escolhidas, o sistema deverá responder qual o boi mais gordo ou mais magro.
Depois, o sistema deve perguntar se o usuário deseja fazer outra ação (daí o menu deverá ser exibido)
ou se deseja fechar o sistema.

Colocar isso no google pode ajudar:
How to open file python
How to write file python
How to load json file python
How to save json file python

2. Um duto flexível (chega de falar de boi) é um tipo de duto usado para extrair petróleo, de forma resumida, é
um longo duto que liga o poço a plataforma. Em seu interior, o petróleo escoa do leito submarino até a
superfície oceânica. Nesse processo, ele deve ser manter integro, seja a uma pressão de 200atm até 1atm,
estando sujeito a diversos tipos de forças dinâmicas submarinas. Essas condições provocaram, ao longo de
muitas décadas, desenvolvimentos científicos para a sua fabricação. Atualmente, esses dutos são formados
por uma parede com várias camadas, cada camada tendo uma espessura e um material de forma que algum
requisito de integridade/eficiência seja atendido. 

Legal, né? Então, a Petrobras precisa de um banco de dados para guardar os dados técnicos dos modelos de
dutos submarinos. Normalmente, chamamos esses “modelos” de estrutura de um duto, essa, por sua vez,
guarda dados que definem como o duto foi fabricado. Por exemplo: guardam qual o diâmetro daquele modelo,
quantas camadas, qual a espessura de cada camada, qual o seu material etc.
Por fim, cabe a você definir como estarão relacionadas todas as entidades de uma estrutura de duto flexível
e enviar para TIC um diagrama Entidade-Relacionamento. Sabendo que:s
Uma estrutura é identificada por um código human-friendly. Exemplo: BJ 151.232.65-0
Uma estrutura é fabricada por um desses fabricantes: TechnipFMC, GE/Wellstream ou DNV.
Uma estrutura tem parâmetros globais:
o Diâmetro interno [pol]
o Condutividade Térmica [W/(m.K)]
o Coeficiente de atrito (aço/aço)
o Lâmina d’água máxima [m]
o Máxima pressão interna diferencial de projeto [MPa]
Uma estrutura tem várias camadas.
Cada camada tem parâmetros:
o Ordem da camada na estrutura [direção radial]
o Se a camada é vedante [true/false]
o Espessura [mm]
o Peso linear [kg/m]
o Tipo do material, que pode ser METAL, POLÍMERO ou COMPÓSITO.
o O seu material de fabricação.
o A sua geometria de fabricação, caso o tipo do material seja um metal.
O material, por sua vez, dispõe também de alguns parâmetros:
o Condutividade térmica do material [W/(m.K)]
o Peso Específico [kgf/m3]
o Fabricante, que pode ser TechnipFMC, GE/Wellstream ou DNV
o Calor Específico [J/(kg.K)]
A geometria tem os parâmetros:
o Espessura do perfil (mm)
o Ângulo dos arames [graus]