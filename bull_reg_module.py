import json
import os
import string
# Módulo para registro de bois.

def check_and_create_file(filename):
    if not os.path.exists(filename):
        with open(filename, 'w') as file:
            json.dump([], file)


# def check_inputs(bull_code, bull_name, bull_mass):
#     return True

def register(filename: string): 

    bull_code = str(input("Código de 4 caracteres do boi: "))
    bull_name = str(input("Nome do boi: "))
    bull_mass = float(input("Massa do boi em quilograma: "))  

    check_and_create_file(filename)

    with open(filename, 'r') as file:
        try:
            bulls = json.load(file)
        except:
            bulls = []

    bulls.append({'code': bull_code, 'name': bull_name, 'mass': bull_mass})

    with open(filename, 'w') as file:
        json.dump(bulls, file)

    print("\nBoi cadastrado.")

# Módulo para comparar e printar para o usuário o boi mais leve.
def compare_thin(filename): 
    check_and_create_file(filename)
    with open(filename, 'r') as file:
        bulls = json.load(file)
    if not bulls:
        print("Não há bois cadastrados")
        return    

    smallest_mass = bulls[0]['mass']
    for index, bull in enumerate(bulls):
        bull_mass = bull['mass']
        if bull_mass <= smallest_mass:
            smallest_mass = bull_mass
            smallest_bull = index

    print("Boi mais leve: {:d} | massa: {:.2f} ".format((smallest_bull+1),smallest_mass))

# Módulo para comparar e printar para o usuário o boi mais pesado.
def compare_fat(filename): 
    check_and_create_file(filename)
    with open(filename, 'r') as file:
        bulls = json.load(file)
    if not bulls:
        print("Não há bois cadastrados")
        return    

    biggest_mass = bulls[0]['mass']
    for index, bull in enumerate(bulls):
        bull_mass = bull['mass']
        if bull_mass >= biggest_mass:
            biggest_mass = bull_mass
            biggest_bull = index

    print("Boi mais pesado: {:d} | massa: {:.2f} ".format((biggest_bull+1),biggest_mass))


def menu(filename):
    print("""
    Menu:
    1. Cadastrar novo boi. 
    2. Calcular boi mais leve. 
    3. Calcular boi mais pesado.
    4. Sair do menu.
    """)
    ans = input("O que o usuário deseja fazer? Tecle o dígito correspondente à escolha: ")
    if ans == "1":
        print("\nCadastrando novo boi: ")
        register(filename)
    elif ans == "2":
      print("\nCalculando o boi mais magro: \n")
      compare_thin(filename)
    elif ans == "3":
        print("\nCalculando o boi mais gordo: \n")
        compare_fat(filename)
    elif ans == "4":
      print("\nGoodbye :(") 
      return True
    else:
       print("\nDígito inválido :/ tente novamente.\n")

    return False
